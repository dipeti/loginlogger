<?php

namespace Dinya\LoginLoggerBundle;

use Doctrine\Bundle\DoctrineBundle\DependencyInjection\Compiler\DoctrineOrmMappingsPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class DinyaLoginLoggerBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        if (class_exists('Doctrine\Bundle\DoctrineBundle\DependencyInjection\Compiler\DoctrineOrmMappingsPass'))
        {
            $container->addCompilerPass(
                DoctrineOrmMappingsPass::createAnnotationMappingDriver(
                    array('Dinya\LoginLoggerBundle\Model'),
                    array(realpath(__DIR__.'/Model')),
                    array(), // doctrine.default_entity_manager is appended automatically
                    false,
                    array('LoginLoggerBundle' => 'Dinya\LoginLoggerBundle\Model') // Map of alias to namespace.
                )
            );
        }
    }
}

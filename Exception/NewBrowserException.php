<?php

namespace Dinya\LoginLoggerBundle\Exception;

use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Throwable;

class NewBrowserException extends AuthenticationException
{
    private $link;

    public function __construct($link, $message = "", $code = 0, Throwable $previous = null)
    {
        $this->link = $link;
        parent::__construct($message, $code, $previous);
    }

    public function getMessageKey()
    {
        return 'You have been sent an email with a link to approve your currently used browser.';
    }

    public function getMessageData()
    {
        return [
            'resend-link' => $this->link,
            'message' => 'If you want us to resend the email click %link_start%here%link_end%.'
        ];
    }

    public function serialize()
    {
        return serialize([
                $this->link,
                parent::serialize(),
        ]);
    }

    public function unserialize($str)
    {
        list($this->link,$parentData) = unserialize($str);
        parent::unserialize($parentData);
    }
}

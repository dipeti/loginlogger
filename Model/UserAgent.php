<?php

namespace Dinya\LoginLoggerBundle\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="dinya_user_agents")
 * @ORM\HasLifecycleCallbacks()
 */
class UserAgent
{
    const AUTHENTICATION_TOKEN_KEY = 'dinya_browser_key';
    const AUTHENTICATION_TOKEN_VALUE = 'dinya_browser_value';
    const COOKIE_TIME_IN_SECONDS = 3600;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="datetime")
     */
    private $lastLogin;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $cookieToken;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $emailToken;

    /**
     * @ORM\Column(type="boolean")
     */
    private $enabled;

    /**
     * @ORM\ManyToOne(targetEntity="Dinya\LoginLoggerBundle\Model\UserInterface", inversedBy="userAgents")
     * @var UserInterface
     */
    private $user;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return UserAgent
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set lastLogin
     *
     * @ORM\PrePersist()
     *
     * @return UserAgent
     */
    public function updateLastLogin()
    {
        $this->lastLogin = new \DateTime('now');

        return $this;
    }

    /**
     * Get lastLogin
     *
     * @return \DateTime
     */
    public function getLastLogin()
    {
        return $this->lastLogin;
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @return UserAgent
     */
    public function setCookieToken($token)
    {
        $this->cookieToken = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getCookieToken()
    {
        return $this->cookieToken;
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @return UserAgent
     */
    public function setEmailToken($token)
    {
        $this->emailToken = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getEmailToken()
    {
        return $this->emailToken;
    }

    /**
     * Set user
     *
     * @param \Dinya\LoginLoggerBundle\Model\UserInterface $user
     *
     * @return UserAgent
     */
    public function setUser(\Dinya\LoginLoggerBundle\Model\UserInterface $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Dinya\LoginLoggerBundle\Model\UserInterface
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return UserAgent
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }
}

<?php

namespace Dinya\LoginLoggerBundle\Model;

interface UserInterface
{
    public function getNumOfFailedLogin();
    public function setNumOfFailedLogin($numOfFailedLogin);
    public function getIpAddresses();
    public function hasIpAddress(IpAddress $ipAddress = null);
    public function addIpAddress(IpAddress $ipAddress = null);
    public function getUserAgents();
    public function hasUserAgent(UserAgent $userAgent = null);
    public function addUserAgent(UserAgent $userAgent = null);
    public function getCookieKey();

    // If you are using the FOSUserBundle these functions are implemented by the FOS\UserBundle\Model\User class
    public function isEnabled();
    public function setEnabled($boolean);
    public function getEmail();
}

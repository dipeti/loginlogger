<?php

namespace Dinya\LoginLoggerBundle\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="dinya_ip_addresses")
 * @ORM\HasLifecycleCallbacks()
 */
class IpAddress
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $address;

    /**
     * @ORM\Column(type="datetime")
     */
    private $lastLogin;

    /**
     * @ORM\Column(type="boolean")
     */
    private $enabled;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $token;

    /**
     * @ORM\ManyToOne(targetEntity="Dinya\LoginLoggerBundle\Model\UserInterface", inversedBy="ipAddresses")
     * @var UserInterface
     */
    private $user;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return IpAddress
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set lastLogin
     *
     * @ORM\PrePersist()
     *
     * @return IpAddress
     */
    public function updateLastLogin()
    {
        $this->lastLogin = new \DateTime('now');

        return $this;
    }

    /**
     * Get lastLogin
     *
     * @return \DateTime
     */
    public function getLastLogin()
    {
        return $this->lastLogin;
    }

    /**
     * Set user
     *
     * @param \Dinya\LoginLoggerBundle\Model\UserInterface $user
     *
     * @return IpAddress
     */
    public function setUser(\Dinya\LoginLoggerBundle\Model\UserInterface $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Dinya\LoginLoggerBundle\Model\UserInterface
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return IpAddress
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @return IpAddress
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }
}

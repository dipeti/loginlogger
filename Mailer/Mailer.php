<?php

namespace Dinya\LoginLoggerBundle\Mailer;

use Dinya\LoginLoggerBundle\Model\UserInterface;
use Symfony\Component\Routing\Router;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Templating\EngineInterface;
use Symfony\Component\Translation\TranslatorInterface;

class Mailer implements MailerInterface
{
    /**
     * @var \Swift_Mailer
     */
    protected $mailer;

    /**
     * @var EngineInterface
     */
    protected $templating;

    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var string
     */
    protected $fromAddress;

    /**
     * @var integer
     */
    protected $possibleAttempts;

    /**
     * Mailer constructor.
     * @param \Swift_Mailer $mailer
     * @param EngineInterface $templating
     */
    public function __construct(
        \Swift_Mailer $mailer,
        EngineInterface $templating,
        RouterInterface $router,
        TranslatorInterface $translator,
        $fromAddress,
        $possibleAttempts
    )
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->router = $router;
        $this->translator = $translator;
        $this->fromAddress = $fromAddress;
        $this->possibleAttempts = $possibleAttempts;
    }

    /**
     * Send an email warn a user of a failed login attempt.
     * TODO: teszteljuk a ket esetet!
     * @param UserInterface $user
     */
    public function sendFailedLoginEmailMessage(UserInterface $user)
    {
        if (0 < $this->possibleAttempts)
        {
            $body = $this->templating->render(
                '@DinyaLoginLogger/email/failed_login_attempt_with_limit.txt.twig',
                [
                    'user' => $user,
                    'num' => $this->possibleAttempts - $user->getNumOfFailedLogin(),
                    'possibleAttempts' => $this->possibleAttempts,
                ]
            );
        }
        else
        {
            $body = $this->templating->render(
                '@DinyaLoginLogger/email/failed_login_attempt.txt.twig',
                [
                    'user' => $user,
                ]
            );
        }

        $message = (new \Swift_Message())
            ->setSubject($this->translator->trans('Unsuccessful login attempt!', [], 'DinyaLoginLoggerBundle'))
            ->setFrom($this->fromAddress)
            ->setTo($user->getEmail())
            ->setBody($body);
        $this->mailer->send($message);
    }

    /**
     * Send an email to warn a user of a successful login attempt from a new IP address.
     *
     * @param UserInterface $user
     * @param $ipAddress
     * @param $userAgent
     */
    public function sendNewIpAddressEmailMessage(UserInterface $user, $ipAddress, $userAgent)
    {
        $message = (new \Swift_Message())
            ->setFrom($this->fromAddress)
            ->setTo($user->getEmail())
            ->setSubject($this->translator->trans('Sign-in from a new IP address: %ip%', array('%ip%' => $ipAddress->getAddress()), 'DinyaLoginLoggerBundle'))
            ->setBody($this->templating->render('@DinyaLoginLogger/email/login_with_new_ip_address.txt.twig',[
                'user' => $user,
                'useragent' => $userAgent,
                'ip' => $ipAddress->getAddress(),
            ]));
        $this->mailer->send($message);
    }

    /**
     * Send an email to ask a user to confirm the new IP address.
     *
     * @param UserInterface $user
     * @param $ipAddress
     * @param $userAgent
     */
    public function sendNewIpAddressEmailMessageWithConfirmation(UserInterface $user, $ipAddress, $userAgent)
    {
        $message = (new \Swift_Message())
            ->setFrom($this->fromAddress)
            ->setTo($user->getEmail())
            ->setSubject($this->translator->trans('Sign-in from a new IP address: %ip%', array('%ip%' => $ipAddress->getAddress()), 'DinyaLoginLoggerBundle'))
            ->setBody($this->templating->render('@DinyaLoginLogger/email/login_with_new_ip_address_confirmation.txt.twig',[
                'user' => $user,
                'useragent' => $userAgent,
                'ip' => $ipAddress->getAddress(),
                'confirmation' => $this->router->generate('dinya_login_logger_confirm_ip_address',['token' => $ipAddress->getToken()],Router::ABSOLUTE_URL)
            ]));
        $this->mailer->send($message);
    }

    /**
     * Send an email to warn a user of a successful login attempt via a new User Agent.
     *
     * @param UserInterface $user
     * @param $ipAddress
     * @param $userAgent
     */
    public function sendNewUserAgentEmailMessage(UserInterface $user, $ipAddress,$userAgent)
    {
        $message = (new \Swift_Message())
            ->setFrom($this->fromAddress)
            ->setTo($user->getEmail())
            ->setSubject('New sign-in from ' . $userAgent . ' browser')
            ->setBody($this->templating->render('@DinyaLoginLogger/email/login_with_new_user_agent.txt.twig',[
                'user' => $user,
                'useragent' => $userAgent,
                'ip' => $ipAddress,
            ]));
        $this->mailer->send($message);
    }

    /**
     * Send an email to ask a user to confirm the new User Agent.
     *
     * @param UserInterface $user
     */
    public function sendNewUserAgentEmailMessageWithConfirmation(UserInterface $user, $ipAddress, $userAgent)
    {
        $message = (new \Swift_Message())
            ->setFrom($this->fromAddress)
            ->setTo($user->getEmail())
            ->setSubject('New sign-in from ' . $userAgent->getName() . ' browser')
            ->setBody($this->templating->render('@DinyaLoginLogger/email/login_with_new_user_agent_confirmation.txt.twig',[
                'user' => $user,
                'useragent' => $userAgent->getName(),
                'ip' => $ipAddress,
                'confirmation' => $this->router->generate('dinya_login_logger_confirm_user_agent',['token' => $userAgent->getEmailToken()],Router::ABSOLUTE_URL),
            ]));
        $this->mailer->send($message);
    }
}

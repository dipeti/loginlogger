<?php

namespace Dinya\LoginLoggerBundle\Mailer;

use Dinya\LoginLoggerBundle\Model\UserInterface;

interface MailerInterface
{
    /**
     * Send an email warn a user of a failed login attempt.
     *
     * @param UserInterface $user
     */
    public function sendFailedLoginEmailMessage(UserInterface $user);

    /**
     * Send an email to warn a user of a successful login attempt from a new IP address.
     *
     * @param UserInterface $user
     */
    public function sendNewIpAddressEmailMessage(UserInterface $user, $ipAddress, $userAgent);

    /**
     * Send an email to ask a user to confirm the new IP address.
     *
     * @param UserInterface $user
     * @param $ipAddress
     * @param $userAgent
     */
    public function sendNewIpAddressEmailMessageWithConfirmation(UserInterface $user, $ipAddress, $userAgent);

    /**
     * Send an email to warn a user of a successful login attempt via a new User Agent.
     *
     * @param UserInterface $user
     */
    public function sendNewUserAgentEmailMessage(UserInterface $user, $ipAddress, $userAgent);

    /**
     * Send an email to ask a user to confirm the new User Agent.
     *
     * @param UserInterface $user
     */
    public function sendNewUserAgentEmailMessageWithConfirmation(UserInterface $user, $ipAddress, $userAgent);
}

<?php

namespace dinya\LoginLoggerBundle\Tests\DependencyInjection;

use Dinya\LoginLoggerBundle\DependencyInjection\DinyaLoginLoggerExtension;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Yaml\Parser;

class DinyaLoginLoggerExtensionTest extends TestCase
{
    /** @var ContainerBuilder */
    protected $configuration;

    //////////////////////////////////
    /**
     * DEFAULT CONFIGURATION TESTS - Services are unavailable unless they are explicitly set.
     */

    /**
     * @expectedException \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     */
    public function testConfigWhenPossibleAttemptsIsNotSet()
    {
        $this->configuration = new ContainerBuilder();
        $loader = new DinyaLoginLoggerExtension();
        $config = array();
        $loader->load(array($config), $this->configuration);
        $this->assertSame($this->getDefaultConfig()['failed_login']['ban_user_after_attempts'], $this->configuration->getParameter('dinya_login_logger.possible_attempts'));
        $this->assertEquals(0,$this->configuration->getParameter('dinya_login_logger.possible_attempts'));
        $this->configuration->getDefinition('dinya_login_logger.listener.log_failed_login_attempt');
    }

    /**
     * @expectedException \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     */
    public function testConfigWhenEmailNotificationIsNotSet()
    {
        $this->configuration = new ContainerBuilder();
        $loader = new DinyaLoginLoggerExtension();
        $config = array();
        $loader->load(array($config), $this->configuration);
        $this->assertFalse($this->getDefaultConfig()['failed_login']['email_notification_to_user']);
        $this->configuration->getDefinition('dinya_login_logger.listener.failed_login_notification_message_attempt');
    }

    /**
     * @expectedException \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     */
    public function testConfigWhenNewBrowserDetectionIsNotSet()
    {
        $this->configuration = new ContainerBuilder();
        $loader = new DinyaLoginLoggerExtension();
        $config = array();
        $loader->load(array($config), $this->configuration);
        $this->assertNull($this->getDefaultConfig()['detect_login_from']['new_browser']);
        $this->configuration->getDefinition('dinya_login_logger.listener.log_new_user_agent');
    }

    /**
     * @expectedException \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     */
    public function testConfigWhenNewIpDetectionIsNotSet()
    {
        $this->configuration = new ContainerBuilder();
        $loader = new DinyaLoginLoggerExtension();
        $config = array();
        $loader->load(array($config), $this->configuration);
        $this->assertNull($this->getDefaultConfig()['detect_login_from']['new_browser']);
        $this->configuration->getDefinition('dinya_login_logger.listener.log_new_ip_address');
    }

    //////////////////////////////////
    /** USER MISCONFIGURATION TESTS */

    /**
     * @expectedException \Symfony\Component\Config\Definition\Exception\InvalidConfigurationException
     */
    public function testConfigWhenUserNonBooleanValueForFailedLoginEmailNotification()
    {
        $this->configuration = new ContainerBuilder();
        $loader = new DinyaLoginLoggerExtension();
        $config = $this->getDefaultConfig();
        $config['failed_login']['email_notification_to_user'] = 'false';// boolean expected
        $loader->load(array($config), $this->configuration);
    }

    /**
     * @expectedException \Symfony\Component\Config\Definition\Exception\InvalidConfigurationException
     */
    public function testConfigWhenUserSetsNegativePossibleAttempts()
    {
        $this->configuration = new ContainerBuilder();
        $loader = new DinyaLoginLoggerExtension();
        $config = $this->getDefaultConfig();
        $config['failed_login']['ban_user_after_attempts'] = -1;// must be greater than or equal to 0
        $loader->load(array($config), $this->configuration);
    }


    /**
     * @expectedException \Symfony\Component\Config\Definition\Exception\InvalidConfigurationException
     */
    public function testConfigWhenUserMistypesConfirmationForBrowserDetection()
    {
        $this->configuration = new ContainerBuilder();
        $loader = new DinyaLoginLoggerExtension();
        $config = $this->getDefaultConfig();
        $config['detect_login_from']['new_browser'] = 'confimation';// typo
        $loader->load(array($config), $this->configuration);
    }

    /**
     * @expectedException \Symfony\Component\Config\Definition\Exception\InvalidConfigurationException
     */
    public function testConfigWhenUserMistypesNotificationForIpDetection()
    {
        $this->configuration = new ContainerBuilder();
        $loader = new DinyaLoginLoggerExtension();
        $config = $this->getDefaultConfig();
        $config['detect_login_from']['new_browser'] = 'notificaiton';// typo
        $loader->load(array($config), $this->configuration);
    }

    //////////////////////////////////
    /** CORRECT CONFIGURATION TESTS */

    public function testFullConfigIfAllServicesAreAvailable()
    {
        $this->configuration = new ContainerBuilder();
        $loader = new DinyaLoginLoggerExtension();
        $config = $this->getFullConfig();
        $loader->load(array($config), $this->configuration);
        $this->assertTrue($this->configuration->hasDefinition('dinya_login_logger.listener.failed_login_notification_message_attempt'));
        $this->assertTrue($this->configuration->hasParameter('dinya_login_logger.possible_attempts'));
        $this->assertTrue($this->configuration->hasDefinition('dinya_login_logger.listener.log_failed_login_attempt'));
        $this->assertTrue($this->configuration->hasDefinition('dinya_login_logger.mailer'));
        $this->assertTrue($this->configuration->hasDefinition('dinya_login_logger.listener.log_successful_login_attempt'));
        $this->assertTrue($this->configuration->hasDefinition('dinya_login_logger.listener.log_new_ip_address'));
        $this->assertTrue($this->configuration->hasDefinition('dinya_login_logger.listener.log_new_user_agent'));
        $this->assertTrue($this->configuration->hasDefinition('dinya_login_logger.listener.send_cookie_listener'));
        $this->assertTrue($this->configuration->hasDefinition('dinya_login_logger.twig_extension.render_link_extension'));

    }
    private function getDefaultConfig()
    {
        $yaml = <<<EOF
failed_login:
    email_notification_to_user: false
    ban_user_after_attempts: 0
detect_login_from:
    new_browser: ~
    new_ip_address: ~
EOF;
        $parser = new Parser();

        return $parser->parse($yaml);

    }

    private function getFullConfig()
    {
        $yaml = <<<EOF
failed_login:
    email_notification_to_user: true
    ban_user_after_attempts: 5
detect_login_from:
    new_browser: confirmation
    new_ip_address: notification
EOF;
        $parser = new Parser();

        return $parser->parse($yaml);

    }
    protected function tearDown()
    {
        unset($this->configuration);
    }

}
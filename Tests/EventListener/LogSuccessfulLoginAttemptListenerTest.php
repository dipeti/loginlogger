<?php

namespace Dinya\LoginLoggerBundle\Tests\EventListener;


use Dinya\LoginLoggerBundle\EventListener\LogSuccessfulLoginAttemptListener;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class LogSuccessfulLoginAttemptListenerTest extends \PHPUnit\Framework\TestCase
{
    /** @var  LogSuccessfulLoginAttemptListener */
    private $listener;
    /** @var  InteractiveLoginEvent */
    private $interactiveLoginEvent;
    /** @var \PHPUnit_Framework_MockObject_MockObject  */
    private $user;

    protected function setUp()
    {
        $entityManager = $this->getMockBuilder('Doctrine\ORM\EntityManagerInterface')->getMock();

        $request = new Request();
        $this->user = $this->getUser();
        $token = $this->getToken();
        $this->interactiveLoginEvent = new InteractiveLoginEvent($request,$token);
        $this->listener = new LogSuccessfulLoginAttemptListener($entityManager);

    }

    /**
     * Tests that after a successful login if the number of failed login equals zero,
     * then it is not restored back to zero unnecessarily.
     */
    public function testResetFailedLoginAttemptsWhenTheNumberOfFailedLoginEqualsZero()
    {
        $this->user
            ->method('getNumOfFailedLogin')
            ->willReturn(0);
        $this->user
            ->expects(self::never())
            ->method('setNumOfFailedLogin');
        $this->listener->resetFailedLoginAttempts($this->interactiveLoginEvent);
    }

    /**
     * Tests that after a successful login if the number of failed login is greater than zero,
     * then it is restored back to zero.
     */
    public function testResetFailedLoginAttemptsWhenTheNumberOfFailedLoginGreaterThenZero()
    {
        $this->user
            ->method('getNumOfFailedLogin')
            ->willReturn(1);
        $this->user
            ->expects(self::once())
            ->method('setNumOfFailedLogin');
        $this->listener->resetFailedLoginAttempts($this->interactiveLoginEvent);
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    private function getUser()
    {
        $user = $this->getMockBuilder('Dinya\LoginLoggerBundle\Tests\DummyUser')->getMock();
        return $user;
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    private function getToken()
    {
        $token = $this->getMockBuilder('Symfony\Component\Security\Core\Authentication\Token\TokenInterface')->getMock();
        $token
            ->method('getUser')
            ->willReturn($this->user);
        return $token;
    }
}
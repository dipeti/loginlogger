<?php

namespace Dinya\LoginLoggerBundle\Tests\EventListener;


use Dinya\LoginLoggerBundle\EventListener\LoginWithNewUserAgentListener;
use Dinya\LoginLoggerBundle\Model\UserAgent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class LoginWithNewUserAgentListenerTest extends \PHPUnit\Framework\TestCase
{
    const CONFIRMATION_REQUIRED = true;
    const COOKIE_KEY = 'cookie-key';
    /** @var  \PHPUnit_Framework_MockObject_MockObject */
    private $entityManager;
    /** @var  \PHPUnit_Framework_MockObject_MockObject */
    private $mailer;
    /** @var  \PHPUnit_Framework_MockObject_MockObject */
    private $router;
    /** @var \PHPUnit_Framework_MockObject_MockObject  */
    private $repository;
    /** @var InteractiveLoginEvent*/
    private $interactiveLoginEvent;
    /** @var FilterUserResponseEvent*/
    private $filterResponseUserEvent;
    /** @var \PHPUnit_Framework_MockObject_MockObject */
    private $user;

    protected function setUp()
    {
        $this->entityManager = $this->getMockBuilder('Doctrine\ORM\EntityManagerInterface')->getMock();
        $this->repository = $this->getMockBuilder('Doctrine\Common\Persistence\ObjectRepository')->getMock();
        $this->entityManager->method('getRepository')->willReturn($this->repository);
        $this->mailer = $this->getMockBuilder('Dinya\LoginLoggerBundle\Mailer\MailerInterface')->getMock();
        $this->router = $this->getMockBuilder('Symfony\Component\Routing\RouterInterface')->getMock();

        $response = $this->getMockBuilder('Symfony\Component\HttpFoundation\Response')->getMock();
        $request = new Request();
        $request->cookies->add([
            md5(self::COOKIE_KEY) => 'value'
        ]);
        $this->user = $this->getUser();
        $token = $this->getToken();
        $this->interactiveLoginEvent = new InteractiveLoginEvent($request,$token);
        $this->filterResponseUserEvent = new FilterUserResponseEvent($this->user,$request,$response);
    }

    /**
     * Tests that when the request does not have the cookie key and the confirmation is enabled,
     * then a NewBrowserException is thrown. (The first if statement holds.)
     * @expectedException \Dinya\LoginLoggerBundle\Exception\NewBrowserException
     */
    public function testCheckUserAgentWhenRequestDoesNotHaveCookieKeyWithConfirmation()
    {
        $this->entityManager
            ->expects(self::never())
            ->method('getRepository');
        $this->mailer
            ->expects(self::once())
            ->method('sendNewUserAgentEmailMessageWithConfirmation');

        $listener = new LoginWithNewUserAgentListener($this->entityManager,$this->mailer,$this->router, self::CONFIRMATION_REQUIRED);

        $listener->checkUserAgent($this->interactiveLoginEvent);
    }

    /**
     * Tests that when the request does not have the cookie key and the confirmation is disabled,
     * then the mailer sends the email without confirmation. (The first if statement holds.)
     */
    public function testCheckUserAgentWhenRequestDoesNotHaveCookieKeyWithoutConfirmation()
    {
        $this->mailer
            ->expects(self::once())
            ->method('sendNewUserAgentEmailMessage');
        $this->entityManager
            ->expects(self::once())
            ->method('flush');

        $listener = new LoginWithNewUserAgentListener($this->entityManager,$this->mailer,$this->router, !self::CONFIRMATION_REQUIRED);

        $listener->checkUserAgent($this->interactiveLoginEvent);
    }

    /**
     * Note: confirmation is not tested from this onwards because it would be redundant.
     * Tests that when the request has the matching cookie key but the useragent was not saved previously, then
     * the useragent is persisted against the database.
     * @expectedException \Dinya\LoginLoggerBundle\Exception\NewBrowserException
     */
    public function testCheckUserAgentWhenRequestHasCookieKeyAndUserAgentIsNotSaved()
    {
        $this->user
            ->method('getCookieKey')
            ->willReturn(self::COOKIE_KEY);

        $this->mailer
            ->expects(self::once())
            ->method('sendNewUserAgentEmailMessageWithConfirmation');

        $listener = new LoginWithNewUserAgentListener($this->entityManager,$this->mailer,$this->router, self::CONFIRMATION_REQUIRED);

        $listener->checkUserAgent($this->interactiveLoginEvent);
    }

    /**
     * Tests that when the useragent is saved, but disabled, then the user gets unauthenticated.
     * @expectedException \Dinya\LoginLoggerBundle\Exception\NewBrowserException
     */
    public function testCheckUserAgentWhenRequestHasCookieKeyAndUserAgentIsDisabled()
    {
        $this->user
            ->method('getCookieKey')
            ->willReturn(self::COOKIE_KEY);

        $userAgent = new UserAgent();
        $userAgent->setEnabled(false);
        $this->repository
            ->method('findOneBy')
            ->willReturn($userAgent);

        $this->entityManager
            ->expects(self::once())
            ->method('flush');

        $listener = new LoginWithNewUserAgentListener($this->entityManager,$this->mailer,$this->router, self::CONFIRMATION_REQUIRED);

        $listener->checkUserAgent($this->interactiveLoginEvent);
    }

    /**
     * Tests when the the useragent is enabled, no exception is thrown and the time of login is updated.
     */
    public function testCheckUserAgentWhenRequestHasCookieKeyAndUserAgentIsEnabled()
    {
        $this->user
            ->method('getCookieKey')
            ->willReturn(self::COOKIE_KEY);

        $userAgent = new UserAgent();
        $userAgent->setEnabled(true);
        $this->repository
            ->method('findOneBy')
            ->willReturn($userAgent);

        $this->entityManager
            ->expects(self::once())
            ->method('flush');

        $listener = new LoginWithNewUserAgentListener($this->entityManager,$this->mailer,$this->router, self::CONFIRMATION_REQUIRED);

        $listener->checkUserAgent($this->interactiveLoginEvent);

    }

    /**
     * Tests when the the useragent is not found, one will be created.
     */
    public function testCheckUserAgentWhenRequestHasCookieKeyAndUserAgentIsNotFound()
    {
        $this->user
            ->method('getCookieKey')
            ->willReturn(self::COOKIE_KEY);

        $this->repository
            ->method('findOneBy')
            ->willReturn(null);

        $this->entityManager
            ->expects(self::once())
            ->method('persist');

        $listener = new LoginWithNewUserAgentListener($this->entityManager,$this->mailer,$this->router, !self::CONFIRMATION_REQUIRED);

        $listener->checkUserAgent($this->interactiveLoginEvent);

    }

    /**
     * Tests that once the registration has completed, the request contains the attributes
     * the SendCookieListener will require when sending the response.
     */
    public function testOnRegistrationCompleted()
    {
        $this->user
            ->method('getCookieKey')
            ->willReturn(self::COOKIE_KEY);

        $listener = new LoginWithNewUserAgentListener($this->entityManager,$this->mailer,$this->router, self::CONFIRMATION_REQUIRED);

        $listener->onRegistrationCompleted($this->filterResponseUserEvent);

        $request = $this->filterResponseUserEvent->getRequest();
        $this->assertTrue($request->attributes->has(UserAgent::AUTHENTICATION_TOKEN_KEY));
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    private function getUser()
    {
        $user = $this->getMockBuilder('Dinya\LoginLoggerBundle\Tests\DummyUser')->getMock();
        return $user;
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    private function getToken()
    {
        $token = $this->getMockBuilder('Symfony\Component\Security\Core\Authentication\Token\TokenInterface')->getMock();
        $token
            ->method('getUser')
            ->willReturn($this->user);
        return $token;
    }
}
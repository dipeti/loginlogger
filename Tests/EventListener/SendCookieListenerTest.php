<?php

namespace Dinya\LoginLoggerBundle\Tests\EventListener;


use Dinya\LoginLoggerBundle\EventListener\SendCookieListener;
use Dinya\LoginLoggerBundle\Model\UserAgent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class SendCookieListenerTest extends \PHPUnit\Framework\TestCase
{
    /** @var Request */
    private $request;
    /** @var Response */
    private $response;
    /** @var SendCookieListener */
    private $listener;
    /** @var  FilterResponseEvent */
    private $filterResponseEvent;

    protected function setUp()
    {
        $httpKernel = $this->getMockBuilder('Symfony\Component\HttpKernel\HttpKernelInterface')->getMock();
        $this->request = new Request();
        $this->response = new Response();
        $this->filterResponseEvent = new FilterResponseEvent($httpKernel,$this->request,HttpKernelInterface::MASTER_REQUEST,$this->response);
        $this->listener = new SendCookieListener();
    }

    /**
     * Tests that when the request does not have the attribute set by the LoginWithNewUserAgentListener,
     * then the Cookie is not set in the response either.
     */
    public function testOnResponseWhenTheRequestDoesNotHaveTheAttribute()
    {
        $this->listener->onResponse($this->filterResponseEvent);
        $this->assertEmpty($this->response->headers->getCookies());

    }

    /**
     * Tests that when the request has the attribute set by the LoginWithNewUserAgentListener,
     * then the Cookie is set in the response.
     */
    public function testOnResponseWhenTheRequestHasTheAttribute()
    {
        $this->request->attributes->set(UserAgent::AUTHENTICATION_TOKEN_KEY,'value');
        $this->listener->onResponse($this->filterResponseEvent);
        $this->assertNotEmpty($this->response->headers->getCookies());
    }
}
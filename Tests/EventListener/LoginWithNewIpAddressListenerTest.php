<?php

namespace Dinya\LoginLoggerBundle\Tests\EventListener;

use Dinya\LoginLoggerBundle\EventListener\LoginWithNewIpAddressListener;
use Dinya\LoginLoggerBundle\Model\IpAddress;
use Dinya\LoginLoggerBundle\Tests\DummyUser;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class LoginWithNewIpAddressListenerTest extends \PHPUnit\Framework\TestCase
{
    const CONFIRMATION_REQUIRED = true;
    /** @var  \PHPUnit_Framework_MockObject_MockObject */
    private $entityManager;
    /** @var  \PHPUnit_Framework_MockObject_MockObject */
    private $mailer;
    /** @var  \PHPUnit_Framework_MockObject_MockObject */
    private $router;
    /** @var \PHPUnit_Framework_MockObject_MockObject  */
    private $repository;
    /** @var InteractiveLoginEvent*/
    private $interactiveLoginEvent;
    /** @var FilterUserResponseEvent*/
    private $filterResponseUserEvent;
    /** @var \PHPUnit_Framework_MockObject_MockObject */
    private $user;


    protected function setUp()
    {
        $this->entityManager = $this->getMockBuilder('Doctrine\ORM\EntityManagerInterface')->getMock();
        $this->repository = $this->getMockBuilder('Doctrine\Common\Persistence\ObjectRepository')->getMock();
        $this->entityManager->method('getRepository')->willReturn($this->repository);
        $this->mailer = $this->getMockBuilder('Dinya\LoginLoggerBundle\Mailer\MailerInterface')->getMock();
        $this->router = $this->getMockBuilder('Symfony\Component\Routing\RouterInterface')->getMock();

        $response = $this->getMockBuilder('Symfony\Component\HttpFoundation\Response')->getMock();
        $request = new Request();
        $this->user = $this->getUser();
        $token = $this->getToken();
        $this->interactiveLoginEvent = new InteractiveLoginEvent($request,$token);
        $this->filterResponseUserEvent = new FilterUserResponseEvent($this->user,$request,$response);
    }

    /**
     * Tests that when a new IP address is used by the user a confirmation email gets sent
     * and the user gets unauthenticated.
     * @expectedException \Dinya\LoginLoggerBundle\Exception\NewIpException
     */
    public function testCheckIpAddressIfIpAddressIsNotSavedWithConfirmationEnabled()
    {
        $listener = new LoginWithNewIpAddressListener($this->entityManager,$this->mailer,$this->router, self::CONFIRMATION_REQUIRED);
        $this->mailer
            ->expects(self::once())
            ->method('sendNewIpAddressEmailMessageWithConfirmation');
        $listener->checkIpAddress($this->interactiveLoginEvent);
    }

    /**
     * Tests that when a new IP address is used by the user a notification email gets sent
     * while the user is successfully authenticated.
     */
    public function testCheckIpAddressIfIpAddressIsNotSavedWithoutConfirmation()
    {
        $listener = new LoginWithNewIpAddressListener($this->entityManager,$this->mailer,$this->router, !self::CONFIRMATION_REQUIRED);
        $this->entityManager
            ->expects(self::once())
            ->method('flush');
        $this->mailer
            ->expects(self::once())
            ->method('sendNewIpAddressEmailMessage');

        $listener->checkIpAddress($this->interactiveLoginEvent);
    }

    /**
     * Tests that while the user had saved this IP address before, it has not been confirmed yet.
     * As a result, a confirmation email gets sent and the user gets unauthenticated.
     * @expectedException \Dinya\LoginLoggerBundle\Exception\NewIpException
     */
    public function testCheckIpAddressIfIpAddressIsDisabledAndTokenIsNull()
    {
        $ipAddress = new IpAddress();
        $ipAddress->setEnabled(false);
        $this->repository ->method('findOneBy')->willReturn($ipAddress);
        $listener = new LoginWithNewIpAddressListener($this->entityManager,$this->mailer,$this->router, self::CONFIRMATION_REQUIRED);
        $this->mailer
            ->expects(self::once())
            ->method('sendNewIpAddressEmailMessageWithConfirmation');

        $listener->checkIpAddress($this->interactiveLoginEvent);
    }

    /**
     * Tests that while the user had saved this IP address before, it has not been confirmed yet.
     * In addition, the token has already been set.
     * As a result, a confirmation email gets sent and the user gets unauthenticated.
     * @expectedException \Dinya\LoginLoggerBundle\Exception\NewIpException
     */
    public function testCheckIpAddressIfIpAddressIsDisabledAndTokenExists()
    {
        $ipAddress = new IpAddress();
        $ipAddress->setEnabled(false);
        $ipAddress->setToken('mock-token');
        $this->repository ->method('findOneBy')->willReturn($ipAddress);
        $listener = new LoginWithNewIpAddressListener($this->entityManager,$this->mailer,$this->router, self::CONFIRMATION_REQUIRED);
        $this->mailer
            ->expects(self::never())
            ->method('sendNewIpAddressEmailMessageWithConfirmation');

        $listener->checkIpAddress($this->interactiveLoginEvent);
    }

    /**
     * Tests that the IP address is saved and enabled. No email gets sent.
     */
    public function testCheckIpAddressIfIpAddressIsEnabled()
    {
        $ipAddress = new IpAddress();
        $ipAddress->setEnabled(true);
        $this->repository ->method('findOneBy')->willReturn($ipAddress);
        $listener = new LoginWithNewIpAddressListener($this->entityManager,$this->mailer,$this->router, self::CONFIRMATION_REQUIRED);
        $this->entityManager
            ->expects(self::once())
            ->method('flush');
        $this->mailer
            ->expects(self::never())
            ->method('sendNewIpAddressEmailMessageWithConfirmation');
        $this->mailer
            ->expects(self::never())
            ->method('sendNewIpAddressEmailMessage');
        $listener->checkIpAddress($this->interactiveLoginEvent);
    }

    /**
     * Tests when the user has completed their registration,
     * the IP address they are using is persisted against the database.
     */
    public function testOnRegistrationCompletedWhenIpAddressDoesNotExist()
    {
        $listener = new LoginWithNewIpAddressListener($this->entityManager,$this->mailer,$this->router, self::CONFIRMATION_REQUIRED);
        $this->user
            ->expects(self::once())
            ->method('addIpAddress');

        $this->entityManager
            ->expects(self::once())
            ->method('flush');

        $listener->onRegistrationCompleted($this->filterResponseUserEvent);
    }

    /**
     * Tests when the user has confirmed their registration,
     * the IP address they are using had already been persisted against the database.
     */
    public function testOnRegistrationCompletedWhenIpAddressDoesExist()
    {
        $listener = new LoginWithNewIpAddressListener($this->entityManager,$this->mailer,$this->router, self::CONFIRMATION_REQUIRED);
        $this->user
            ->expects(self::never())
            ->method('addIpAddress');

        $ipAddress = $this->getMockBuilder('Dinya\LoginLoggerBundle\Model\IpAddress')->getMock();
        $this->repository ->method('findOneBy')->willReturn($ipAddress);

        $this->entityManager
            ->expects(self::once())
            ->method('flush');

        $listener->onRegistrationCompleted($this->filterResponseUserEvent);
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    private function getUser()
    {
        $user = $this->getMockBuilder('Dinya\LoginLoggerBundle\Tests\DummyUser')->getMock();
        return $user;
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    private function getToken()
    {
        $token = $this->getMockBuilder('Symfony\Component\Security\Core\Authentication\Token\TokenInterface')->getMock();
        $token
            ->expects(self::any())
            ->method('getUser')
            ->willReturn($this->user);
        return $token;
    }
}

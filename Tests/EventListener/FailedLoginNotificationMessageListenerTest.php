<?php

namespace Dinya\LoginLoggerBundle\Tests\EventListener;

use Dinya\LoginLoggerBundle\EventListener\FailedLoginNotificationMessageListener;
use Symfony\Component\Security\Core\Event\AuthenticationFailureEvent;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class FailedLoginNotificationMessageListenerTest extends \PHPUnit\Framework\TestCase
{
    /** @var AuthenticationFailureEvent */
    private $event;
    /** @var FailedLoginNotificationMessageListener */
    private $listener;
    /** @var \PHPUnit_Framework_MockObject_MockObject */
    private $provider;
    /** @var \PHPUnit_Framework_MockObject_MockObject */
    private $mailer;

    protected function setUp()
    {
        $entityManager = $this->getMockBuilder('Doctrine\ORM\EntityManagerInterface')->getMock();
        $this->mailer = $this->getMockBuilder('Dinya\LoginLoggerBundle\Mailer\MailerInterface')->getMock();

        $this->provider = $this->getUserProvider();

        $this->listener = new FailedLoginNotificationMessageListener($entityManager,$this->mailer,$this->provider);

        $token = $this->getMockBuilder('Symfony\Component\Security\Core\Authentication\Token\TokenInterface')->getMock();
        $authException = new AuthenticationException();
        $this->event = new AuthenticationFailureEvent($token, $authException);
    }

    /**
     * Tests that the email does not get sent when the user is not found.
     */
    public function testSendNotificationEmailIfUserIsNotFound()
    {
        $this->provider
            ->method('loadUserbyUsername')
            ->willReturn(null);

        $this->mailer
            ->expects(self::never())
            ->method('sendFailedLoginEmailMessage');

        $this->listener->sendNotificationEmail($this->event);
    }

    /**
     * Tests that the email does not get sent when the user disabled.
     */
    public function testSendNotificationEmailIfUserIsFoundAndDisabled()
    {
        $user = $this->getUser();
        $user
            ->method('isEnabled')
            ->willReturn(false);

        $this->provider
            ->method('loadUserbyUsername')
            ->willReturn($user);

        $this->mailer
            ->expects(self::never())
            ->method('sendFailedLoginEmailMessage');

        $this->listener->sendNotificationEmail($this->event);
    }
    /**
     * Tests that the email gets sent when the user is found and is enabled.
     */
    public function testSendNotificationEmailIfUserIsFoundAndEnabled()
    {
        $user = $this->getUser();
        $user
            ->method('isEnabled')
            ->willReturn(true);

        $this->provider
            ->method('loadUserbyUsername')
            ->willReturn($user);

        $this->mailer
            ->expects(self::once())
            ->method('sendFailedLoginEmailMessage');

        $this->listener->sendNotificationEmail($this->event);
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    private function getUser()
    {
        return $this->getMockBuilder('Dinya\LoginLoggerBundle\Model\UserInterface')->getMock();
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    private function getUserProvider()
    {
        return $this->getMockBuilder('Symfony\Component\Security\Core\User\UserProviderInterface')->getMock();
    }
}
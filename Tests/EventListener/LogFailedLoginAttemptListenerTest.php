<?php

namespace Dinya\LoginLoggerBundle\Tests\EventListener;


use Dinya\LoginLoggerBundle\EventListener\LogFailedLoginAttemptListener;
use Symfony\Component\Security\Core\Event\AuthenticationFailureEvent;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
// TODO: tobb eset van, mindet teszteljuk.
class LogFailedLoginAttemptListenerTest extends \PHPUnit\Framework\TestCase
{

    const NUMBER_OF_POSSIBLE_ATTEMPTS = 5;
    /** @var AuthenticationFailureEvent */
    private $event;
    /** @var LogFailedLoginAttemptListener */
    private $listener;
    /** @var \PHPUnit_Framework_MockObject_MockObject */
    private $provider;
    /** @var \PHPUnit_Framework_MockObject_MockObject */
    private $mailer;
    /** @var  \PHPUnit_Framework_MockObject_MockObject */
    private $entityManager;

    protected function setUp()
    {
        $this->entityManager = $this->getMockBuilder('Doctrine\ORM\EntityManagerInterface')->getMock();
        $this->mailer = $this->getMockBuilder('Dinya\LoginLoggerBundle\Mailer\MailerInterface')->getMock();

        $this->provider = $this->getUserProvider();

        $this->listener = new LogFailedLoginAttemptListener($this->entityManager,$this->mailer,$this->provider,self::NUMBER_OF_POSSIBLE_ATTEMPTS);

        $token = $this->getMockBuilder('Symfony\Component\Security\Core\Authentication\Token\TokenInterface')->getMock();
        $authException = new AuthenticationException();
        $this->event = new AuthenticationFailureEvent($token, $authException);
    }

    /**
     * Tests that the email does not get sent when the user is not found.
     */
    public function testAddFailedLoginAttemptIfUserIsNotFound()
    {
        $this->provider
            ->method('loadUserbyUsername')
            ->willReturn(null);
        $this->entityManager
            ->expects(self::never())
            ->method('flush');
        $this->mailer
            ->expects(self::never())
            ->method('sendFailedLoginEmailMessage');

        $this->listener->addFailedLoginAttempt($this->event);
    }

    /**
     * Tests that the email does not get sent when the user is disabled.
     */
    public function testAddFailedLoginAttemptIfUserIsDisabled()
    {
        $user = $this->getUser();
        $user
            ->method('isEnabled')
            ->willReturn(false);
        $user
            ->expects(self::never())
            ->method('getNumOfFailedLogin');

        $this->provider
            ->method('loadUserbyUsername')
            ->willReturn($user);

        $this->listener->addFailedLoginAttempt($this->event);
    }

    /**
     * Tests that the email does not get sent if the
     * current number of failed login attempts of the user
     * does not exceed {@link NUMBER_OF_POSSIBLE_ATTEMPTS}.
     */
    public function testAddFailedLoginAttemptIfNumOfFailedLoginIsFewerThanPermittedAttempts()
    {
        $user = $this->getUser();
        $user
            ->method('isEnabled')
            ->willReturn(true);
        $user
            ->expects(self::exactly(2))
            ->method('getNumOfFailedLogin')
            ->willReturn(self::NUMBER_OF_POSSIBLE_ATTEMPTS - 1);

        $this->provider
            ->method('loadUserbyUsername')
            ->willReturn($user);

        $this->mailer
            ->expects(self::never())
            ->method('sendFailedLoginEmailMessage');

        $this->listener->addFailedLoginAttempt($this->event);
    }

    /**
     * Tests that the email gets sent if the
     * current number of failed login attempts of the user is
     * greater than or equal to {@link NUMBER_OF_POSSIBLE_ATTEMPTS}.
     */
    public function testAddFailedLoginAttemptIfNumOfFailedLoginIsGreaterThanOrEqualToPermittedAttempts()
    {
        $user = $this->getUser();
        $user
            ->method('isEnabled')
            ->willReturn(true);
        $user
            ->method('getNumOfFailedLogin')
            ->willReturn(self::NUMBER_OF_POSSIBLE_ATTEMPTS);

        $this->provider
            ->method('loadUserbyUsername')
            ->willReturn($user);

        $this->mailer
            ->expects(self::once())
            ->method('sendFailedLoginEmailMessage');

        $this->listener->addFailedLoginAttempt($this->event);
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    private function getUser()
    {
        return $this->getMockBuilder('Dinya\LoginLoggerBundle\Model\UserInterface')->getMock();
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    private function getUserProvider()
    {
        return $this->getMockBuilder('Symfony\Component\Security\Core\User\UserProviderInterface')->getMock();
    }
}
<?php

namespace Dinya\LoginLoggerBundle\Tests\Form;

use Dinya\LoginLoggerBundle\Form\UserAgentType;
use Dinya\LoginLoggerBundle\Model\UserAgent;
use Symfony\Component\Form\Test\TypeTestCase;

class UserAgentTypeTest extends TypeTestCase
{
    public function testSubmit()
    {
        $userAgent = new UserAgent();
        $userAgent->setEnabled(true);

        $form = $this->factory->create(UserAgentType::class, $userAgent);
        $form->submit([
            'enabled' => 0
        ]);

        $this->assertTrue($form->isSynchronized());
        $this->assertSame($userAgent, $form->getData());
        $this->assertFalse($userAgent->isEnabled());
    }
}
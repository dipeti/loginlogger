<?php
namespace Dinya\LoginLoggerBundle\Tests\Form;

use Dinya\LoginLoggerBundle\Form\IpAddressType;
use Dinya\LoginLoggerBundle\Model\IpAddress;
use Symfony\Component\Form\Test\TypeTestCase;

class IpAddressTypeTest extends TypeTestCase
{
    public function testSubmit()
    {
        $ipAddress = new IpAddress();
        $ipAddress->setEnabled(true);

        $form = $this->factory->create(IpAddressType::class, $ipAddress);
        $form->submit([
            'enabled' => 0
        ]);

        $this->assertTrue($form->isSynchronized());
        $this->assertSame($ipAddress, $form->getData());
        $this->assertFalse($ipAddress->isEnabled());
    }
}
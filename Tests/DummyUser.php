<?php

namespace Dinya\LoginLoggerBundle\Tests;

use Dinya\LoginLoggerBundle\Model\IpAddress;
use Dinya\LoginLoggerBundle\Model\UserAgent;

class DummyUser extends \FOS\UserBundle\Model\User implements \Dinya\LoginLoggerBundle\Model\UserInterface
{
    public function getNumOfFailedLogin(){}

    public function setNumOfFailedLogin($numOfFailedLogin){}

    public function getIpAddresses(){}

    public function hasIpAddress(IpAddress $ipAddress = null){}

    public function addIpAddress(IpAddress $ipAddress = null){}

    public function getUserAgents(){}

    public function hasUserAgent(UserAgent $userAgent = null){}

    public function addUserAgent(UserAgent $userAgent = null){}

    public function getCookieKey(){}
}
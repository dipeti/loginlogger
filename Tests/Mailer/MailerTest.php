<?php

namespace Dinya\LoginLoggerBundle\Tests\Mailer;


use Dinya\LoginLoggerBundle\Mailer\Mailer;
use PHPUnit\Framework\TestCase;

class MailerTest extends TestCase
{
    /** @var Mailer */
    private $mailer;
    const POSSIBLE_ATTEMPTS = 5;
    const POSSIBLE_ATTEMPTS_NOT_SET = 0;

    public function testSendNewUserAgentEmailMessageWithConfirmation()
    {
        $this->mailer = $this->getMailer();
        $this->mailer->sendNewUserAgentEmailMessageWithConfirmation($this->getUser(),'127.0.0.1',$this->getUserAgent());
    }

    public function testSendNewUserAgentEmailMessage()
    {
        $this->mailer = $this->getMailer();
        $this->mailer->sendNewUserAgentEmailMessage($this->getUser(),'127.0.0.1','DinyaUserAgent');
        self::assertTrue(true);
    }

    public function testSendNewIpAddressEmailMessage()
    {
        $this->mailer = $this->getMailer();
        $this->mailer->sendNewIpAddressEmailMessage($this->getUser(),$this->getIpAddress(), 'DinyaUserAgent');
        self::assertTrue(true);
    }

    /**
     * Tests that if the number of possible attempts are set,
     * then the number of failed logins are requested from the user.
     */
    public function testSendFailedLoginEmailMessageIfPossibleAttemptsIsNotZero()
    {
        $this->mailer = $this->getMailer();
        $user = $this->getUser();
        $user->expects(self::once())
            ->method('getNumOfFailedLogin')
            ->willReturn(self::POSSIBLE_ATTEMPTS);
        $this->mailer->sendFailedLoginEmailMessage($user);
    }

    /**
     * Tests that if the number of possible attempts is not set,
     * then the number of failed logins are requested from the user.
     */
    public function testSendFailedLoginEmailMessageIfPossibleAttemptsIsZero()
    {
        $this->mailer = $this->getMailerWithoutPossibleAttemptsCounter();
        $user = $this->getUser();
        $user->expects(self::never())
            ->method('getNumOfFailedLogin');
        $this->mailer->sendFailedLoginEmailMessage($user);
    }

    public function testSendNewIpAddressEmailMessageWithConfirmation()
    {
        $this->mailer = $this->getMailer();
        $this->mailer->sendNewIpAddressEmailMessageWithConfirmation($this->getUser(), $this->getIpAddress(), 'DinyaUserAgent');
    }

    private function getMailer()
    {
        return new Mailer(
            new \Swift_Mailer(
                new \Swift_Transport_NullTransport(
                    $this->getMockBuilder('\Swift_Events_EventDispatcher')->getMock()
                )
            ),
            $this->getTemplating(),
            $this->getMockBuilder('Symfony\Component\Routing\RouterInterface')->getMock(),
            $this->getMockBuilder('Symfony\Component\Translation\TranslatorInterface')->getMock(),
            'john@doe.com',
            self::POSSIBLE_ATTEMPTS
        );
    }

    private function getMailerWithoutPossibleAttemptsCounter()
    {
        return new Mailer(
            new \Swift_Mailer(
                new \Swift_Transport_NullTransport(
                    $this->getMockBuilder('\Swift_Events_EventDispatcher')->getMock()
                )
            ),
            $this->getTemplating(),
            $this->getMockBuilder('Symfony\Component\Routing\RouterInterface')->getMock(),
            $this->getMockBuilder('Symfony\Component\Translation\TranslatorInterface')->getMock(),
            'john@doe.com',
            self::POSSIBLE_ATTEMPTS_NOT_SET
        );
    }

    private function getUser()
    {
        $user = $this->getMockBuilder('Dinya\LoginLoggerBundle\Model\UserInterface')->getMock();
        $user->method('getEmail')
            ->willReturn('user@dinya.hu');

        return $user;
    }

    private function getUserAgent()
    {
        $userAgent = $this->getMockBuilder('Dinya\LoginLoggerBundle\Model\UserAgent')->getMock();
        $userAgent
            ->expects(self::atLeastOnce())
            ->method('getName')
            ->willReturn('DinyaUserAgent');

        return $userAgent;
    }

    private function getTemplating()
    {
        $templating = $this->getMockBuilder('Symfony\Bundle\FrameworkBundle\Templating\EngineInterface')
            ->disableOriginalConstructor()
            ->getMock()
        ;

        return $templating;
    }

    private function getIpAddress()
    {
        $ipAddress = $this->getMockBuilder('Dinya\LoginLoggerBundle\Model\IpAddress')->getMock();
        $ipAddress
            ->expects(self::atLeastOnce())
            ->method('getAddress')
            ->willReturn('127.0.0.1');

        return $ipAddress;
    }


}
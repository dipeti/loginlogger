<?php

namespace Dinya\LoginLoggerBundle\Form;

use Dinya\LoginLoggerBundle\Model\IpAddress;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class IpAddressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('enabled',ChoiceType::class,[
                'choices' => [
                    'Enabled' => true,
                    'Disabled' => false,
                ],
                'label' => false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => IpAddress::class]);
    }

    public function getBlockPrefix()
    {
        return 'dinya_login_logger_bundle_ip_address_type';
    }
}

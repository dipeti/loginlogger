<?php

namespace Dinya\LoginLoggerBundle\Twig\Extension;

use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Translation\Translator;

class RenderLinkExtension extends \Twig_Extension
{
    private $translator;

    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('render_link', [$this, 'renderLink'], ['is_safe' => ['html']]),
        ];
    }

    public function renderLink($error)
    {
        if (!($error instanceof  AuthenticationException))
        {
            throw new \InvalidArgumentException('Error has to be an instance of AuthenticationException.');
        }

        $data = $error->getMessageData();
        if (isset($data['message']) && isset($data['resend-link']))
        {
            $link = $this->translator->trans(
                $data['message'],
                [
                    '%link_start%' => '<a href="'.$data['resend-link'].'">',
                    '%link_end%' => '</a>',
                ],
                'security'
            );
            return $link;
        }
        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'render_link';
    }
}

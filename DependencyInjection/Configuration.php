<?php

namespace Dinya\LoginLoggerBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('atmtk_login_logger');

        $supportedEmails = [null, 'notification', 'confirmation'];

        $rootNode
            ->children()
                ->arrayNode('failed_login')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->booleanNode('email_notification_to_user')->defaultFalse()->end()// true or false
                        ->integerNode('ban_user_after_attempts')->min(0)->defaultValue(0)->end()// 0 (disabled) or integer (1 <= enabled)
                    ->end()
                ->end()
                ->arrayNode('detect_login_from')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('new_browser')
                            ->validate()->ifNotInArray($supportedEmails)->thenInvalid('The email %s is not supported. Please choose one of '.json_encode($supportedEmails))->end()
                            ->defaultNull()->end()// null or 'notification' or 'confirmation'
                        ->scalarNode('new_ip_address')
                            ->validate()->ifNotInArray($supportedEmails)->thenInvalid('The email %s is not supported. Please choose one of '.json_encode($supportedEmails))->end()
                            ->defaultNull()->end()// null or 'notification' or 'confirmation'
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}

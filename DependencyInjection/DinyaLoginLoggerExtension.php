<?php

namespace Dinya\LoginLoggerBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\Alias;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class DinyaLoginLoggerExtension extends Extension implements PrependExtensionInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {

        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));

        $loader->load('mailer.yml');
        $loader->load('render_link.yml');

        // Have to set it, mailer and log_failed_login_attempt is depending on this.
        $container->setParameter(
            'dinya_login_logger.possible_attempts',
            $config['failed_login']['ban_user_after_attempts']
        );

        if ($config['failed_login']['email_notification_to_user'])
        {
            $loader->load('failed_login_notification_message.yml');
        }
        if (0 < $config['failed_login']['ban_user_after_attempts'])
        {
            $loader->load('failed_login_attempt.yml');
            $loader->load('log_successful_login_attempt.yml');
        }

        if (!is_null($config['detect_login_from']['new_browser']))
        {
            $confirmation_required = $config['detect_login_from']['new_browser'] === 'confirmation' ? true : false;
            $container->setParameter('dinya_login_logger.browser_confirmation_required', $confirmation_required);

            $loader->load('log_new_user_agent.yml');
            $loader->load('send_cookie_listener.yml');
        }
        if (!is_null($config['detect_login_from']['new_ip_address']))
        {
            $confirmation_required = $config['detect_login_from']['new_ip_address'] === 'confirmation' ? true : false;
            $container->setParameter('dinya_login_logger.ip_confirmation_required', $confirmation_required);

            $loader->load('log_new_ip_address.yml');
        }
    }

    /**
     * Allow an extension to prepend the extension configurations.
     *
     * @param ContainerBuilder $container
     */
    public function prepend(ContainerBuilder $container)
    {
        $this->setCheckPath($container);
        $this->setFormLoginProvider($container);
    }

    /**
     * Sets an alias for the currently used FormLoginProvider.
     */
    private function setFormLoginProvider(ContainerBuilder $container)
    {
        $firewallName =  $container->getExtensionConfig('fos_user')[0]['firewall_name'];
        $providerKey = $container->getExtensionConfig('security')[0]['firewalls'][$firewallName]['form_login']['provider'];
        $providers = $container->getExtensionConfig('security')[0]['providers'];
        $formProvider = null;
        foreach ($providers as $key => $provider)
        {
            if ($key===$providerKey)
            {
                $formProvider = $provider['id'];
            }
        }
        $alias = 'dinya_login_logger.current_user_provider';
        if ($formProvider)
        {
            $container->setAlias($alias,new Alias($formProvider));
        }
        else
        {   $defaultProvider = 'fos_user.user_provider.username';
            $container->setAlias($alias,new Alias($defaultProvider));
        }

        return null;
    }

    /**
     * Sets a parameter for the currently used form_login check_path.
     */
    private function setCheckPath(ContainerBuilder $container)
    {
        $alias = 'dinya_login_logger.login_check_path';
        $firewallName =  $container->getExtensionConfig('fos_user')[0]['firewall_name'];
        if (isset($container->getExtensionConfig('security')[0]['firewalls'][$firewallName]['form_login']['check_path']))
        {
            $container->setParameter($alias,$container->getExtensionConfig('security')[0]['firewalls'][$firewallName]['form_login']['check_path']);
        }
        else
        {
            $container->setParameter($alias,'/login_check');
        }
    }
}

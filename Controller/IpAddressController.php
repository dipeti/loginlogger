<?php

namespace Dinya\LoginLoggerBundle\Controller;

use Dinya\LoginLoggerBundle\Form\IpAddressType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class IpAddressController extends Controller
{
    /**
     * @Route("/ipaddress/confirm", name="dinya_login_logger_confirm_ip_address")
     */
    public function confirmAction(Request $request)
    {
        $translator = $this->get('translator');

        if ($request->query->has('token'))
        {
            $token = $request->query->get('token');
            $repo = $this->getDoctrine()->getRepository('LoginLoggerBundle:IpAddress');

            $ipAddress = $repo->findOneBy(['token' => $token]);
            if ($ipAddress && !$ipAddress->isEnabled())
            {
                $ipAddress->setEnabled(true);
                $ipAddress->setToken(null);
                $this->getDoctrine()->getManager()->flush();

                $flash = $translator->trans('Ip address successfully confirmed.', [], 'DinyaLoginLoggerBundle');
                $this->addFlash('success', $flash);

                return $this->redirectToRoute('fos_user_security_login');
            }
        }

        $flash = $translator->trans('This token is invalid.', [], 'DinyaLoginLoggerBundle');
        $this->addFlash('warning',$flash);

        return $this->redirectToRoute('fos_user_security_login');
    }

    /**
     * @Route("/ipaddress/resend", name="dinya_login_logger_resend_ip_address")
     */
    public function resendConfirmationAction(Request $request)
    {
        $translator = $this->get('translator');

        if ($request->query->has('token'))
        {
            $token = $request->query->get('token');
            $repo = $this->getDoctrine()->getRepository('LoginLoggerBundle:IpAddress');

            $ipAddress = $repo->findOneBy(['token' => $token]);
            if ($ipAddress)
            {
                $userAgent = $request->headers->get('User-Agent');
                $ipAddress->setToken(sha1(date(DATE_COOKIE).$ipAddress->getId()));
                $this->getDoctrine()->getManager()->flush();

                $this->get('dinya_login_logger.mailer')
                    ->sendNewIpAddressEmailMessageWithConfirmation(
                        $ipAddress->getUser(),
                        $ipAddress,
                        $userAgent
                    );

                $flash = $translator->trans('Confirmation email has been resent.', [], 'DinyaLoginLoggerBundle');
                $this->addFlash('success', $flash);

                return $this->redirectToRoute('fos_user_security_login');
            }
        }

        $flash = $translator->trans('This token is invalid.', [], 'DinyaLoginLoggerBundle');
        $this->addFlash('warning', $flash);

        return $this->redirectToRoute('fos_user_security_login');
    }

    /**
     * @Route("/ipaddress/show", name="dinya_login_logger_show_ip_address")
     */
    public function showAction(Request $request)
    {
        $user = $this->getUser();
        $form = $this->createFormBuilder($user)
            ->add('ipaddresses', CollectionType::class, [
                'entry_type' => IpAddressType::class,
            ])
            ->add('submit', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('dinya_login_logger_show_ip_address');
        }

        return $this->render('@DinyaLoginLogger/ipAddress/show_ip.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}

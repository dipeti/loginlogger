<?php

namespace Dinya\LoginLoggerBundle\Controller;

use AppBundle\Entity\User;
use Dinya\LoginLoggerBundle\Form\UserAgentType;
use Dinya\LoginLoggerBundle\Model\UserAgent;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class UserAgentController extends Controller
{
    /**
     * @Route("/useragent/confirm", name="dinya_login_logger_confirm_user_agent")
     */
    public function confirmAction(Request $request)
    {
        $translator = $this->get('translator');

        if ($request->query->has('token'))
        {
            $token = $request->query->get('token');
            $repo = $this->getDoctrine()->getRepository('LoginLoggerBundle:UserAgent');

            $userAgent = $repo->findOneBy(['emailToken' => $token]);
            if ($userAgent && !$userAgent->isEnabled())
            {
                $userAgent->setEnabled(true);
                $userAgent->setEmailToken(null);
                $this->getDoctrine()->getManager()->flush();

                $flash = $translator->trans('Browser successfully confirmed.', [], 'DinyaLoginLoggerBundle');
                $this->addFlash('success', $flash);

                return $this->redirectToRoute('fos_user_security_login');
            }
        }

        $flash = $translator->trans('This token is invalid.', [], 'DinyaLoginLoggerBundle');
        $this->addFlash('warning', $flash);

        return $this->redirectToRoute('fos_user_security_login');
    }

    /**
     * @Route("/useragent/resend", name="dinya_login_logger_resend_user_agent")
     */
    public function resendConfirmationAction(Request $request)
    {
        $translator = $this->get('translator');

        if ($request->query->has('token'))
        {
            $token = $request->query->get('token');
            $repo = $this->getDoctrine()->getRepository('LoginLoggerBundle:UserAgent');

            $userAgent = $repo->findOneBy(['emailToken' => $token]);
            if ($userAgent)
            {
                $ipAddress = $request->getClientIp();
                $userAgent->setEmailToken(sha1(date(DATE_COOKIE).$userAgent->getId()));
                $this->getDoctrine()->getManager()->flush();

                $this->get('Dinya_login_logger.mailer')
                    ->sendNewUserAgentEmailMessageWithConfirmation(
                        $userAgent->getUser(),
                        $ipAddress,
                        $userAgent
                    );

                $flash = $translator->trans('Confirmation email has been resent.', [], 'DinyaLoginLoggerBundle');
                $this->addFlash('success', $flash);

                return $this->redirectToRoute('fos_user_security_login');
            }
        }

        $flash = $translator->trans('This token is invalid.', [], 'DinyaLoginLoggerBundle');
        $this->addFlash('warning', $flash);

        return $this->redirectToRoute('fos_user_security_login');
    }

    /**
     * @Route("/useragent/show", name="dinya_login_logger_show_user_agent")
     */
    public function showAction(Request $request)
    {
        $user = $this->getUser();
        $form = $this->createFormBuilder($user)
            ->add('useragents', CollectionType::class, [
                'entry_type' => UserAgentType::class,
            ])
            ->add('submit', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('dinya_login_logger_show_user_agent');
        }

        return $this->render('@DinyaLoginLogger/userAgent/show_user_agent.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}

<?php

namespace Dinya\LoginLoggerBundle\EventListener;

use Dinya\LoginLoggerBundle\Mailer\MailerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\AuthenticationEvents;
use Symfony\Component\Security\Core\Event\AuthenticationFailureEvent;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class FailedLoginNotificationMessageListener implements EventSubscriberInterface
{
    private $em;
    private $mailer;
    private $provider;

    public function __construct(
        EntityManagerInterface $em,
        MailerInterface $mailer,
        UserProviderInterface $provider
    )
    {
        $this->em = $em;
        $this->mailer = $mailer;
        $this->provider = $provider;
    }

    public static function getSubscribedEvents()
    {
        /**
         * Necessary to make sure that this service gets executed
         * after 'log_failed_login_attempt' service.
         * Therefore, the priority is set to -10.
         */
        return [
            AuthenticationEvents::AUTHENTICATION_FAILURE => ['sendNotificationEmail', -10]
        ];
    }

    /**
     * Sends a notification email to the user only if the user is found and enabled.
     * Otherwise, no email is sent.
     * @param AuthenticationFailureEvent $event
     */
    public function sendNotificationEmail(AuthenticationFailureEvent $event)
    {
        $username = $event->getAuthenticationToken()->getUser();
        $user = $this->provider->loadUserByUsername($username);
        if ($user && $user->isEnabled())
        {
            $this->mailer->sendFailedLoginEmailMessage($user);
        }
    }
}

<?php

namespace Dinya\LoginLoggerBundle\EventListener;

use Dinya\LoginLoggerBundle\Model\UserAgent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class SendCookieListener implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::RESPONSE => ['onResponse',-10]
        ];
    }

    public function onResponse(FilterResponseEvent $event)
    {
        $request = $event->getRequest();
        if (!$request->attributes->has(UserAgent::AUTHENTICATION_TOKEN_KEY))
        {
            return;
        }

        $token = $request->attributes->get(UserAgent::AUTHENTICATION_TOKEN_VALUE);
        $key = $request->attributes->get(UserAgent::AUTHENTICATION_TOKEN_KEY);
        $response = $event->getResponse();
        $response->headers->setCookie(new Cookie($key,$token,time() + UserAgent::COOKIE_TIME_IN_SECONDS));
    }
}

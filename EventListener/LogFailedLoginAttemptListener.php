<?php

namespace Dinya\LoginLoggerBundle\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use Dinya\LoginLoggerBundle\Mailer\MailerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\AuthenticationEvents;
use Symfony\Component\Security\Core\Event\AuthenticationFailureEvent;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class LogFailedLoginAttemptListener implements EventSubscriberInterface
{
    private $em;
    private $mailer;
    private $provider;
    private $possibleAttempts;

    public function __construct(
        EntityManagerInterface $em,
        MailerInterface $mailer,
        UserProviderInterface $provider,
        $possibleAttempts
    )
    {
        $this->em = $em;
        $this->mailer = $mailer;
        $this->provider = $provider;
        $this->possibleAttempts = $possibleAttempts;
    }

    public static function getSubscribedEvents()
    {
        return [
            AuthenticationEvents::AUTHENTICATION_FAILURE => ['addFailedLoginAttempt', 0],
        ];
    }

    public function addFailedLoginAttempt(AuthenticationFailureEvent $event)
    {
        $username = $event->getAuthenticationToken()->getUser();
        $user = $this->provider->loadUserByUsername($username);

        if ($user && $user->isEnabled())
        {
            $failedLogins = $user->getNumOfFailedLogin();
            $user->setNumOfFailedLogin($failedLogins + 1);

            if ($this->possibleAttempts <= $user->getNumOfFailedLogin())
            {
                $user->setEnabled(false);

                // FailedLoginNotificationMessageListener is not sending emails
                // if the user is disabled, so we have to send the email here
                // in case of disabling the user.
                $this->mailer->sendFailedLoginEmailMessage($user);
            }

            $this->em->flush();
        }
    }
}

<?php

namespace Dinya\LoginLoggerBundle\EventListener;

use Dinya\LoginLoggerBundle\Exception\NewIpException;
use Dinya\LoginLoggerBundle\Mailer\MailerInterface;
use Dinya\LoginLoggerBundle\Model\IpAddress;
use Doctrine\ORM\EntityManagerInterface;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;

class LoginWithNewIpAddressListener implements EventSubscriberInterface
{
    private $em;
    private $mailer;
    private $router;
    private $isConfirmationRequired;

    public function __construct(
        EntityManagerInterface $em,
        MailerInterface $mailer,
        RouterInterface $router,
        $isConfirmationRequired
    )
    {
        $this->em = $em;
        $this->mailer = $mailer;
        $this->router = $router;
        $this->isConfirmationRequired = $isConfirmationRequired;
    }

    public static function getSubscribedEvents()
    {
        /**
         * The IP Address is persisted BOTH when the registration is completed (i.e. the user correctly filled out the form)
         * and - if necessary - when the registration is confirmed.
         * The user might use different IP addresses to complete these tasks.
         */
        return [
            SecurityEvents::INTERACTIVE_LOGIN => 'checkIpAddress',
            FOSUserEvents::REGISTRATION_COMPLETED => 'onRegistrationCompleted',
            FOSUserEvents::REGISTRATION_CONFIRMED => 'onRegistrationCompleted',
        ];
    }

    /**
     * Each time the user is logging in, this is fired (if enabled in config).
     */
    public function checkIpAddress(InteractiveLoginEvent $event)
    {
        $user = $event->getAuthenticationToken()->getUser();
        $request = $event->getRequest();

        $userAgent = $request->headers->get('User-Agent');// browser
        $remoteAddress = $request->server->get('REMOTE_ADDR');// IP address
        $repo = $this->em->getRepository('LoginLoggerBundle:IpAddress');

        // Searching for if this IP address has already been saved by the user.
        $ipAddress = $repo->findOneBy(['address' => $remoteAddress, 'user' => $user]);
        if (null == $ipAddress)
        {
            // CASE 1: The user did NOT save this IP address yet.

            // Saving IP address for this user.
            $ipAddress = new IpAddress();
            $ipAddress->setAddress($remoteAddress);
            $ipAddress->setUser($user);
            $user->addIpAddress($ipAddress);
            $this->em->persist($ipAddress);

            if ($this->isConfirmationRequired)
            {
                // Confirmation is required

                // We generate a token for this IP address.
                $ipAddress->setEnabled(false);
                $ipAddress->setToken(self::generateHash($user));
                $this->em->flush();

                // Sending this token to the user.
                $this->mailer->sendNewIpAddressEmailMessageWithConfirmation(
                    $user,
                    $ipAddress,
                    $userAgent
                );

                // Preparing the link for the NewIpException.
                $link = $this->router->generate(
                    'dinya_login_logger_resend_ip_address',
                    ['token' => $ipAddress->getToken()]
                );

                // We do NOT let the user to login.
                throw new NewIpException($link);
            }// if ($this->isConfirmationRequired)

            // Confirmation is NOT required, we automatically enable the IP address.
            $ipAddress->setEnabled(true);

            // Than we just send an email to notify the user about the new IP address.
            $this->mailer->sendNewIpAddressEmailMessage(
                $user,
                $ipAddress,
                $userAgent
            );

            // We let the user to login (no exception).
        }// if (null == $ipAddress)
        else if (!$ipAddress->isEnabled())
        {
            /**
             * CASE 2: The user did save this IP address,
             * BUT it has not been confirmed yet,
             * OR it had been confirmed but then disabled by the user manually.
             */

            /**
             * If the user confirmed this IP address, then the token is set to null.
             * The user can disable an IP address manually.
             * This is why we need to check if there is a token value stored.
             */
            if (null === $ipAddress->getToken())
            {
                /**
                 * If there is no token value stored, then we generate a new one.
                 * This is the case when the user disabled the IP address manually.
                 */
                $ipAddress->setToken(self::generateHash($user));
                $this->em->flush();

                /**
                 * We can send immediately the confirmation email,
                 * because this IP was disabled by the user.
                 */
                $this->mailer->sendNewIpAddressEmailMessageWithConfirmation(
                    $user,
                    $ipAddress,
                    $userAgent
                );
            }// if (null === $ipAddress->getToken())

            // Preparing the link for the NewIpException.
            $link = $this->router->generate(
                'dinya_login_logger_resend_ip_address',
                ['token' => $ipAddress->getToken()]
            );

            // We do NOT let the user to login.
            throw new NewIpException($link);
        }// else if (!$ipAddress->isEnabled())

        // CASE 3: The user stored this IP address, and it is confirmed.

        $ipAddress->updateLastLogin();
        $this->em->flush();

        // We let the user to login (no exception).
    }

    public function onRegistrationCompleted(FilterUserResponseEvent $event)
    {
        $user = $event->getUser();
        $request = $event->getRequest();
        $remoteAddress = $request->server->get('REMOTE_ADDR');// IP address

        /**
         * Find the IpAddress or if not found, create a new one
         */
        $repo = $this->em->getRepository('LoginLoggerBundle:IpAddress');
        $ipAddress = $repo->findOneBy(['address' => $remoteAddress, 'user' => $user]);
        if (null === $ipAddress)
        {
            // not only does this branch run when the registration is completed, but also when the confirmation is carried out from
            $ipAddress = new IpAddress();
            $ipAddress->setAddress($remoteAddress);
            $ipAddress->setEnabled(true);

            /**
             * Add IpAddress to the user and persist
             */
            $user->addIpAddress($ipAddress);
            $this->em->persist($ipAddress);
        }
        $ipAddress->updateLastLogin();
        $this->em->flush();
    }

    private static function generateHash(\FOS\UserBundle\Model\UserInterface $user)
    {
        return sha1(date(DATE_ATOM).$user->getId().openssl_random_pseudo_bytes(8));
    }
}

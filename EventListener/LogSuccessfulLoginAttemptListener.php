<?php

namespace Dinya\LoginLoggerBundle\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\UserEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;

class LogSuccessfulLoginAttemptListener implements EventSubscriberInterface
{

    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public static function getSubscribedEvents()
    {
        return [
            SecurityEvents::INTERACTIVE_LOGIN => 'resetFailedLoginAttempts',
            FOSUserEvents::RESETTING_RESET_COMPLETED => 'resetFailedLoginAttemptsOnPasswordReset',
        ];
    }

    public function resetFailedLoginAttempts(InteractiveLoginEvent $event)
    {
        $user = $event->getAuthenticationToken()->getUser();

        if (0 < $user->getNumOfFailedLogin())
        {
            $user->setNumOfFailedLogin(0);
            $this->em->flush();
        }
    }

    public function resetFailedLoginAttemptsOnPasswordReset(UserEvent $event)
    {
        $user = $event->getUser();

        if (0 < $user->getNumOfFailedLogin())
        {
            $user->setNumOfFailedLogin(0);
            $this->em->flush();
        }
    }
}

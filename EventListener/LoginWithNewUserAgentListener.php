<?php

namespace Dinya\LoginLoggerBundle\EventListener;

use Dinya\LoginLoggerBundle\Exception\NewBrowserException;
use Dinya\LoginLoggerBundle\Mailer\MailerInterface;
use Dinya\LoginLoggerBundle\Model\UserAgent;
use Doctrine\ORM\EntityManagerInterface;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;

class LoginWithNewUserAgentListener implements EventSubscriberInterface
{
    private $em;
    private $mailer;
    private $router;
    private $isConfirmationRequired;


    public function __construct(EntityManagerInterface $em, MailerInterface $mailer, RouterInterface $router, $isConfirmationRequired)
    {
        $this->em = $em;
        $this->mailer = $mailer;
        $this->router = $router;
        $this->isConfirmationRequired = $isConfirmationRequired;
    }

    public static function getSubscribedEvents()
    {
        /**
         * The UserAgent is persisted BOTH when the registration is completed (i.e. the user correctly filled out the form)
         * and - if necessary - when the registration is confirmed.
         * The user might use different UserAgents to complete these tasks.
         */
        return [
            SecurityEvents::INTERACTIVE_LOGIN => 'checkUserAgent',
            FOSUserEvents::REGISTRATION_COMPLETED => 'onRegistrationCompleted',
            FOSUserEvents::REGISTRATION_CONFIRMED => 'onRegistrationCompleted',
        ];
    }
    public function checkUserAgent(InteractiveLoginEvent $event)
    {
        $user = $event->getAuthenticationToken()->getUser();
        $request = $event->getRequest();
        $ipAddress = $request->server->get('REMOTE_ADDR');

        if (!$request->cookies->has(self::getCookieKey($user)))
        {
            $this->createUserAgent($request, $user, $ipAddress);
            $this->em->flush();
        }
        else
        {
            $token = $request->cookies->get(self::getCookieKey($user));
            $repo = $this->em->getRepository('LoginLoggerBundle:UserAgent');

            $userAgent = $repo->findOneBy(['cookieToken' => $token, 'user' => $user]);
            if (!$userAgent)
            {
                $this->createUserAgent($request, $user, $ipAddress);
            }
            elseif (!$userAgent->isEnabled())
            {
                if (null === $userAgent->getEmailToken())
                {
                    $userAgent->setEmailToken(self::generateHash($user));
                    $this->em->flush();
                }

                $link = $this->router->generate(
                    'dinya_login_logger_resend_user_agent',
                    ['token' => $userAgent->getEmailToken()]
                );

                throw new NewBrowserException($link);// needs confirmation
            }
            else
            {
                $userAgent->updateLastLogin();
            }

            $this->em->flush();
        }
    }
    public function onRegistrationCompleted(FilterUserResponseEvent $event)
    {
        $request = $event->getRequest();
        $user = $event->getUser();

        $token = $request->cookies->get(self::getCookieKey($user));
        $repo = $this->em->getRepository('LoginLoggerBundle:UserAgent');
        $userAgent = null;
        if (null !== $token)
        {
            $userAgent = $repo->findOneBy(['cookieToken' => $token, 'user' => $user]);
        }
        if (null === $userAgent)
        {
            $userAgent = $this->getNewUserAgent($request,$user);
            $userAgent->setEnabled(true);
            $this->em->persist($userAgent);
            $this->em->flush();

            $request->attributes->set(UserAgent::AUTHENTICATION_TOKEN_VALUE,$userAgent->getCookieToken());
            $request->attributes->set(UserAgent::AUTHENTICATION_TOKEN_KEY,self::getCookieKey($user));
        }

    }

    private function createUserAgent($request,$user,$ipAddress)
    {
        $userAgent = $this->getNewUserAgent($request,$user);
        $this->em->persist($userAgent);

        $request->attributes->set(UserAgent::AUTHENTICATION_TOKEN_VALUE,$userAgent->getCookieToken());
        $request->attributes->set(UserAgent::AUTHENTICATION_TOKEN_KEY,self::getCookieKey($user));
        // TODO: Ha ki van kapcsolva a confirmation, akkor nem flusholunk. Attol meg elmenti?
        if ($this->isConfirmationRequired)
        {
            $userAgent->setEmailToken(self::generateHash($user));
            $this->em->flush();

            $this->mailer->sendNewUserAgentEmailMessageWithConfirmation(
                $user,
                $ipAddress,
                $userAgent
            );

            $link = $this->router->generate(
                'dinya_login_logger_resend_user_agent',
                ['token' => $userAgent->getEmailToken()]
            );

            throw new NewBrowserException($link);
        }
        $this->mailer->sendNewUserAgentEmailMessage(
            $user,
            $ipAddress,
            $userAgent->getName()
        );
    }
    private function getNewUserAgent($request, $user)
    {
        $name = $request->server->get('HTTP_USER_AGENT');

        $userAgent = new UserAgent();
        $userAgent->setCookieToken(self::generateHash($user));
        $userAgent->setUser($user);
        $userAgent->setName($name);
        $userAgent->setEnabled(!$this->isConfirmationRequired);
        $user->addUserAgent($userAgent);

        return $userAgent;
    }

    private static function generateHash(UserInterface $user)
    {
        return sha1(date(DATE_COOKIE).$user->getId().openssl_random_pseudo_bytes(8));
    }

    private static function getCookieKey(\Dinya\LoginLoggerBundle\Model\UserInterface $user)
    {
       return md5($user->getCookieKey());
    }
}

DinyaLoginLoggerBundle
======================

DinyaLoginLoggerBundle adds support for banning a user account if three consecutive unsuccessful
login attempts occur.

Features
--------

- If the number of failed login attempts reaches its limit, the user will be permanently suspended.
- If an unsuccessful login attempt occurs and the user is stored in the DB with the given credentials (e.g. username) then an email is to be sent to the associated address.
- If the user succeeds in logging in the number of failed login attempts is restored to zero in the DB.

Installation
------------

1. DinyaLoginLoggerBundle must be enabled by registering them in the registerBundles() method of the AppKernel class.
The Bundle is dependent on the FOSUserBundle:

        $bundles = [
            // ...
            new FOS\UserBundle\FOSUserBundle(),
            new Dinya\LoginLoggerBundle\DinyaLoginLoggerBundle()
            // ...

2. DinyaLoginLoggerBundle uses two services. These services must be included in the app/config/config.yml:

        atmtk_login_logger:
            failed_login:
                email_notification_to_user: true
                ban_user_after_attempts: 3
            detect_login_from:
                new_browser: "confirmation" // or "notification"
                new_ip_address: "confirmation" // or "notification"

3. Create a User class which implements the DinyaLoginLoggerBundle:Model:UserInterface interface.
Here is an example:

        <?php

        namespace AppBundle\Entity;

        use Dinya\LoginLoggerBundle\Model\IpAddress;
        use Dinya\LoginLoggerBundle\Model\UserAgent;
        use Dinya\LoginLoggerBundle\Model\UserInterface;
        use Doctrine\Common\Collections\ArrayCollection;
        use FOS\UserBundle\Model\User as BaseUser;
        use Doctrine\ORM\Mapping as ORM;

        /**
         * @ORM\Entity
         * @ORM\Table(name="fos_user")
         */
        class User extends BaseUser implements UserInterface
        {
            /**
             * @ORM\Id
             * @ORM\Column(type="integer")
             * @ORM\GeneratedValue(strategy="AUTO")
             */
            protected $id;

            /**
             * @ORM\Column(type="integer")
             */
            protected $numOfFailedLogin = 0;

            /**
             * @ORM\OneToMany(targetEntity="Dinya\LoginLoggerBundle\Model\IpAddress", mappedBy="user")
             */
            protected $ipAddresses;

            /**
             * @ORM\OneToMany(targetEntity="Dinya\LoginLoggerBundle\Model\UserAgent", mappedBy="user")
             */
            protected $userAgents;

            public function __construct()
            {
                parent::__construct();
                $this->ipAddresses = new ArrayCollection();
                $this->userAgents = new ArrayCollection();
            }

            /**
             * Set numOfFailedLogin
             *
             * @param integer $numOfFailedLogin
             *
             * @return User
             */
            public function setNumOfFailedLogin($numOfFailedLogin)
            {
                $this->numOfFailedLogin = $numOfFailedLogin;

                return $this;
            }

            /**
             * Get numOfFailedLogin
             *
             * @return integer
             */
            public function getNumOfFailedLogin()
            {
                return $this->numOfFailedLogin;
            }

            /**
             * @return ArrayCollection
             */
            public function getIpAddresses()
            {
               return $this->ipAddresses;
            }

            /**
             * @param $ipAddress
             *
             * @return bool
             */
            public function hasIpAddress(IpAddress $ipAddress = null)
            {
                return $this->ipAddresses->contains($ipAddress);
            }

            /**
             * @param $ipAddress
             *
             * @return User
             */
            public function addIpAddress(IpAddress $ipAddress = null)
            {
                if(!$this->ipAddresses->contains($ipAddress))
                {
                    $ipAddress->setUser($this);
                    $this->ipAddresses->add($ipAddress);
                }
                return $this;
            }

            /**
             * @param ArrayCollection $ipAddresses
             *
             * @return User
             */
            public function setIpAddresses($ipAddresses)
            {
                $this->ipAddresses = $ipAddresses;

                return $this;
            }

            /**
             * @return ArrayCollection
             */
            public function getUserAgents()
            {
                return $this->userAgents;
            }

            /**
             * @param $userAgent
             *
             * @return bool
             */
            public function hasUserAgent(UserAgent $userAgent = null)
            {
                return $this->userAgents->contains($userAgent);
            }

            /**
             * @param UserAgent $userAgent
             *
             * @return User
             */
            public function addUserAgent(UserAgent $userAgent = null)
            {
                if (!$this->userAgents->contains($userAgent))
                {
                    $userAgent->setUser($this);
                    $this->userAgents->add($userAgent);
                }
                return $this;
            }

            /**
             * @param ArrayCollection $userAgents
             *
             * @return User
             */
            public function setUserAgents($userAgents)
            {
                $this->userAgents = $userAgents;

                return $this;
            }

            /**
             * Remove ipAddress
             *
             * @param \Dinya\LoginLoggerBundle\Model\IpAddress $ipAddress
             */
            public function removeIpAddress(\Dinya\LoginLoggerBundle\Model\IpAddress $ipAddress)
            {
                $this->ipAddresses->removeElement($ipAddress);
            }

            /**
             * Remove userAgent
             *
             * @param \Dinya\LoginLoggerBundle\Model\UserAgent $userAgent
             */
            public function removeUserAgent(\Dinya\LoginLoggerBundle\Model\UserAgent $userAgent)
            {
                $this->userAgents->removeElement($userAgent);
            }

            public function getCookieKey()
            {
                return $this->username;
            }
        }

4. Tell Doctrine to resolve the UserInterface to the class we created above in
   ```app/config/config.yml```.
   (See also: [resolve_target_entities documentation](http://symfony.com/doc/current/doctrine/resolve_target_entity.html))

        doctrine:
            orm:
                resolve_target_entities:
                    Dinya\LoginLoggerBundle\Model\UserInterface: AppBundle\Entity\User

5. Configure routing in routing.yml
   (This example assumes there is an "http_protocol" field in parameters.yml.)

        # LoginLoggerBundle
        login_logger:
            resource: "@DinyaLoginLoggerBundle/Controller/"
            type:     annotation
            schemes:  ["%http_protocol%"]


6. Configure mail parameters in the parameters.yml

Tests
-----

1. Pull LoginLoggerBundle from the remote repository.
2. Run `composer install`
3. Go to the project root folder and run `phpunit`  
If PHPUnit is not installed globally on your machine then run `vendor/phpunit/phpunit/phpunit`
